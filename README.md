Burdick Open Quick Change Tool Post
====

This is a tool post design styled after the venerable Norman Patent quick change tool post. It is released under an open hardware license to promote sharing and adapting. It is sized to fit the Sieg C2 lathe, but is easily adapted to other sizes.

Currently the design contains:

1. a 25 mm diameter tool post
2. tool holder for turning and facing with 1/4" tooling
3. tool holder for parting with P1 blades

License
========
This documentation describes Open Hardware and is licensed under the CERN OHL v. 1.2.

You may redistribute and modify this documentation under the terms of the CERN OHL v.1.2. (http://ohwr.org/cernohl). This documentation is distributed WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY, SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see the CERN OHL v.1.1 for applicable conditions.
